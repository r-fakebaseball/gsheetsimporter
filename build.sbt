name := "gsheetsimporter"
version := "1.0"

lazy val `gsheetsimporter` = project in file(".")

scalaVersion := "2.13.0"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.12",
  "com.typesafe.play" %% "play-json" % "2.7.4",
  "com.google.apis" % "google-api-services-sheets" % "v4-rev581-1.25.0",
  "com.google.oauth-client" % "google-oauth-client-jetty" % "1.30.1"
)
