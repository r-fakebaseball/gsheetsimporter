package xyz.redditball.gsheetsimporter

import java.io.{File, FileInputStream, InputStreamReader}
import java.sql.{DriverManager, ResultSet}
import java.util.Collections

import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.{GoogleAuthorizationCodeFlow, GoogleClientSecrets}
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.{HttpRequest, HttpRequestInitializer}
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.sheets.v4.model._
import com.google.api.services.sheets.v4.{Sheets, SheetsScopes}

import scala.collection.mutable
import scala.jdk.CollectionConverters._
import scala.sys.env
import scala.util.Try
import scala.util.chaining._

// This thing is a little script that migrates
// all the DB info from the DB onto Google Sheets
// so it's not gonna be as neat or organized
object Main extends App {

  /*
grant select (discord,id,reddit_name) on users to 'fb-ro'@'%';
grant select on batting_stats to 'fb-ro'@'%';
grant select on batting_types to 'fb-ro'@'%';
grant select on game_actions to 'fb-ro'@'%';
grant select on game_awards to 'fb-ro'@'%';
grant select on game_states to 'fb-ro'@'%';
grant select on games to 'fb-ro'@'%';
grant select on lineup_entries to 'fb-ro'@'%';
grant select on lineups to 'fb-ro'@'%';
grant select on parks to 'fb-ro'@'%';
grant select on pitching_bonuses to 'fb-ro'@'%';
grant select on pitching_stats to 'fb-ro'@'%';
grant select on pitching_types to 'fb-ro'@'%';
grant select on players to 'fb-ro'@'%';
grant select on scoring_plays to 'fb-ro'@'%';
grant select (id, tag, name, park, discord_role, color_discord, color_roster, color_roster_bg, visible, all_players, logo_url) on teams to 'fb-ro'@'%';
   */
  val SQL_HOST = env("SQL_HOST")
  val SQL_SCHEMA = env("SQL_SCHEMA")

  val GOOGLE_SHEETS_SHEET_ID = env("BACKEND_SHEET_ID")
  val SEASON = env("SEASON").toInt

  val SHEET_ID_PLAYERS = env("SHEET_ID_PLAYERS").toInt
  val SHEET_ID_PARKS = env("SHEET_ID_PARKS").toInt
  val SHEET_ID_GAMES = env("SHEET_ID_GAMES").toInt
  val SHEET_ID_GAME_AWARDS = env("SHEET_ID_GAME_AWARDS").toInt
  val SHEET_ID_GAME_LOGS = env("SHEET_ID_GAME_LOGS").toInt
  val SHEET_ID_GAME_STATES = env("SHEET_ID_GAME_STATES").toInt
  val SHEET_ID_PITCHING_STATS = env("SHEET_ID_PITCHING_STATS").toInt
  val SHEET_ID_BATTING_STATS = env("SHEET_ID_BATTING_STATS").toInt
  val SHEET_ID_TEAMS = env("SHEET_ID_TEAMS").toInt
  val SHEET_ID_TYPES = env("SHEET_ID_TYPES").toInt

  def resSetToSheet(sheet: Int, col: Int, row: Int, results: ResultSet, end: Option[(Int, Int)] = None): Seq[Request] = {
    val colCount = res.getMetaData.getColumnCount
    val rows = mutable.Buffer[RowData]()
    while (results.next()) {
      rows += new RowData().setValues((1 to colCount).map(col => {
        val value = results.getString(col)
        val numValue = if (value != null) value.toIntOption.map(new java.lang.Double(_)).orNull else null
        if (numValue != null)
          new CellData().setUserEnteredValue(new ExtendedValue().setNumberValue(numValue))
        else
          new CellData().setUserEnteredValue(new ExtendedValue().setStringValue(value))
      }).asJava)
    }

    if (end.isEmpty) {
      val emptyRow = (1 to colCount).map(_ => new CellData().setUserEnteredValue(new ExtendedValue().setStringValue(""))).asJava
      while (rows.length < 999) {
        rows += new RowData().setValues(emptyRow)
      }
    }

    var index = -1
    rows.grouped(5000).map { rows =>
      index += 1
      new Request()
        .setUpdateCells(
          new UpdateCellsRequest()
            .setRows(rows.asJava)
            .setFields("*")
            .setRange(
              new GridRange()
                .setSheetId(sheet)
                .setStartColumnIndex(index * col)
                .setStartRowIndex(index * 5000 + row)
                .setEndColumnIndex(end.map(i => new Integer(i._1)).orNull)
                .setEndRowIndex(end.map(i => new Integer(i._2)).orNull)
            )
        )
    }.toSeq
  }

  // First get a GSheets token from whoever runs this
  val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
  val JSON_FACTORY = JacksonFactory.getDefaultInstance
  val SECRETS = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(new FileInputStream("./credentials.json")))
  val SCOPES = Seq(SheetsScopes.DRIVE_FILE, SheetsScopes.DRIVE, SheetsScopes.SPREADSHEETS).asJava
  val flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, SECRETS, SCOPES)
    .setDataStoreFactory(new FileDataStoreFactory(new File("tokens")))
    .setAccessType("offline")
    .build()
  val receiver = new LocalServerReceiver.Builder()
    .setPort(8888)
    .build()
  val INITIALIZER = new HttpRequestInitializer {
    override def initialize(request: HttpRequest): Unit = {
      new AuthorizationCodeInstalledApp(flow, receiver)
        .authorize("user")
        .initialize(request)
      request.setConnectTimeout(30 * 1000)
      request.setReadTimeout(5 * 60 * 1000)
    }
  }

  // Open connection
  val conn = DriverManager.getConnection(s"jdbc:mysql://$SQL_HOST/$SQL_SCHEMA?useSSL=false&allowPublicKeyRetrieval=true", "fb-ro", "fb-ro-access")
  val stmt = conn.createStatement()

  // Get all players
  var res = stmt.executeQuery("SELECT a.id,a.name,a.team,a.batting_type,a.pitching_type,a.pitching_bonus,a.right_handed,a.position_primary,a.position_secondary,a.position_tertiary,b.reddit_name FROM players a, users b WHERE a.user = b.id")
  val playersReq = resSetToSheet(SHEET_ID_PLAYERS, 0, 1, res)

  // And MLR teams
  res = stmt.executeQuery("SELECT id,name,tag,park,color_discord,color_roster,color_roster_bg FROM teams")
  val teamsReq = resSetToSheet(SHEET_ID_TEAMS, 0, 1, res)

  // Parks
  res = stmt.executeQuery("SELECT id,name,factor_hr,factor_3b,factor_2b,factor_1b,factor_bb FROM parks")
  val parksReq = resSetToSheet(SHEET_ID_PARKS, 0, 1, res)

  // Player types
  res = stmt.executeQuery("SELECT id,name,shortcode,range_hr,range_3b,range_2b,range_1b,range_bb,range_fo,range_k,range_po,range_rgo,range_lgo FROM batting_types")
  val btReq = resSetToSheet(SHEET_ID_TYPES, 0, 2, res, Some(13, 16))
  res = stmt.executeQuery("SELECT id,name,shortcode,range_hr,range_3b,range_2b,range_1b,range_bb,range_fo,range_k,range_po,range_rgo,range_lgo FROM pitching_types")
  val ptReq = resSetToSheet(SHEET_ID_TYPES, 0, 19, res, Some(13, 33))
  res = stmt.executeQuery("SELECT id,name,shortcode,range_hr,range_3b,range_2b,range_1b,range_bb,range_fo,range_k,range_po,range_rgo,range_lgo FROM pitching_bonuses")
  val pbReq = resSetToSheet(SHEET_ID_TYPES, 0, 36, res, Some(13, 40))

  // Games
  res = stmt.executeQuery(s"SELECT id,id36,season,session,away_team,home_team,state,completed FROM games WHERE season = $SEASON")
  val gamesReq = resSetToSheet(SHEET_ID_GAMES, 0, 1, res)

  // Game States
  res = stmt.executeQuery(s"SELECT id,away_batting_position,home_batting_position,r1,r2,r3,r1_responsibility,r2_responsibility,r3_responsibility,outs,inning,score_away,score_home FROM game_states WHERE id IN ((SELECT state FROM games WHERE season = $SEASON) UNION (SELECT before_state FROM game_actions WHERE game_id IN (SELECT id FROM games WHERE season = $SEASON)) UNION (SELECT after_state FROM game_actions WHERE game_id IN (SELECT id FROM games WHERE season = $SEASON)))")
  val statesReq = resSetToSheet(SHEET_ID_GAME_STATES, 0, 1, res)

  // Game Logs
  res = stmt.executeQuery(s"SELECT id,replaced_by,game_id,play_type,pitcher,pitch,batter,swing,diff,result,runs_scored,scorers,outs_tracked,before_state,after_state FROM game_actions WHERE (SELECT id FROM games WHERE id=game_id AND season = $SEASON) IS NOT NULL")
  val actionsReq = resSetToSheet(SHEET_ID_GAME_LOGS, 0, 1, res)

  // Game Awards (WP, LP, PotG, SV)
  res = stmt.executeQuery(s"SELECT game,winning_pitcher,losing_pitcher,player_of_the_game,save FROM game_awards WHERE (SELECT id FROM games WHERE id=game AND season = $SEASON AND (SELECT id FROM teams WHERE id=away_team) IS NOT NULL) IS NOT NULL")
  val awards = resSetToSheet(SHEET_ID_GAME_AWARDS, 0, 1, res)

  // Stats
  res = stmt.executeQuery(s"SELECT player,season,total_pas,total_outs,total_er,total_hr,total_3b,total_2b,total_1b,total_bb,total_fo,total_k,total_po,total_rgo,total_lgo,total_sb,total_cs,total_dp,total_tp,total_gp,total_sac FROM pitching_stats WHERE season = $SEASON")
  val pStatsReq = resSetToSheet(SHEET_ID_PITCHING_STATS, 0, 1, res)
  res = stmt.executeQuery(s"SELECT player,season,total_pas,total_abs,total_hr,total_3b,total_2b,total_1b,total_bb,total_fo,total_k,total_po,total_rgo,total_lgo,total_rbi,total_r,total_sb,total_cs,total_dp,total_tp,total_gp,total_sac from batting_stats WHERE season = $SEASON")
  val bStatsReq = resSetToSheet(SHEET_ID_BATTING_STATS, 0, 1, res)

  // Now submit the updates to Google Sheets
  val sheets = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, INITIALIZER)
    .setApplicationName("FakeBaseball GSheets Importer/0.1")
    .build()

  val allReqs = Map(
    "Players" -> playersReq,
    "Teams" -> teamsReq,
    "Parks" -> parksReq,
    "Batting Types" -> btReq,
    "Pitching Types" -> ptReq,
    "Pitching Bonuses" -> pbReq,
    "Games" -> gamesReq,
    "Game States" -> statesReq,
    "Game Actions" -> actionsReq,
    "Game Awards" -> awards,
    "Pitching Stats" -> pStatsReq,
    "Batting Stats" -> bStatsReq
  )

  private def tryReq(req: Request): Try[BatchUpdateSpreadsheetResponse] = Try {
    sheets
      .spreadsheets()
      .batchUpdate(GOOGLE_SHEETS_SHEET_ID, new BatchUpdateSpreadsheetRequest().setRequests(Seq(req).asJava))
      .execute()
  }

  val maxNameLen = allReqs.keys.maxBy(_.length).length + 5
  for((name, reqSeq) <- allReqs) {
    for ((req, index) <- reqSeq.zipWithIndex) {

      var success = false
      var failCount = 0

      while (!success && failCount < 3) {
        print(s"$name ${index + 1}".padTo(maxNameLen, '.'))
        tryReq(req).fold(
          error => println(s"FAILED ${failCount += 1; failCount}/3: ${error.getMessage}"),
          response => {success = true; println(response)}
        )

        Thread.sleep(15000)
      }

    }
  }

}
